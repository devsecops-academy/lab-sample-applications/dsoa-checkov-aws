resource "aws_lb" "webapp" {
  name        = "webapp-alb"
  load_balancer_type = "application"
  subnets            = [data.aws_subnet.main.*.id]
  security_groups    = [aws_security_group.webapp-lb.id]
  drop_invalid_header_fields = true
  enable_deletion_protection = true
  access_logs {
    bucket  = aws_lb.webapp
    prefix  = "webapp-alb"
    enabled = true
  }
}

resource "aws_wafregional_ipset" "ipset" {
  name = "tfIPSet"

  ip_set_descriptor {
    type  = "IPV4"
    value = "192.0.7.0/24"
  }
}

resource "aws_wafregional_rule" "foo" {
  name        = "tfWAFRule"
  metric_name = "tfWAFRule"

  predicate {
    data_id = aws_wafregional_ipset.ipset.id
    negated = false
    type    = "IPMatch"
  }
}

resource "aws_wafregional_web_acl" "foo" {
  name        = "foo"
  metric_name = "foo"

  default_action {
    type = "ALLOW"
  }

  rule {
    action {
      type = "BLOCK"
    }

    priority = 1
    rule_id  = aws_wafregional_rule.foo.id
  }

  logging_configuration {
    log_destination = aws_wafregional_web_acl.foo.arn

    redacted_fields {
      field_to_match {
        type = "URI"
      }

      field_to_match {
        data = "referer"
        type = "HEADER"
      }
    }
  }
}


resource "aws_wafregional_web_acl_association" "webapp" {
  resource_arn = aws_lb.webapp.arn
  web_acl_id   = aws_wafregional_web_acl.foo.id
}

resource "aws_lb_target_group" "webapp" {
  name     = "webapp"
  port     = 443
  protocol = "HTTPS"
  vpc_id   = data.aws_vpc.main.id
  
  health_check {
    path = "/api/1/resolve/default?path=/service/my-service"
    matcher = "200"  # has to be HTTP 200 or fails
  }
}

resource "aws_lb_listener" "webapp" {
  load_balancer_arn = aws_lb.webapp.arn
  port       = "443"
  protocol   = "HTTPS"
  ssl_policy = "ELBSecurityPolicy-FS-1-2-Res-2020-10"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.webapp.arn
  }
}


resource "aws_security_group" "webapp-lb" {
    name        = "webapp-lb"
    description = "Allow web app traffic"

    ingress {
        description = "Web app traffic from everywhere"
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        description = "Web app traffic to ASG"
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

}
